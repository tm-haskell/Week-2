module HW02 where
import Debug.Trace

sumFl :: [Integer] -> Integer
sumFl  = foldl (+) 0
  
productFr :: [Integer] -> Integer
productFr = foldr (*) 1

concatFr :: [Int] -> [Int] -> [Int]
concatFr xs ys = foldr (:) ys xs

sortInsert :: [Int] -> [Int]
sortInsert  = foldl insertSorted []

insertSorted :: [Int] -> Int -> [Int] 
insertSorted sorted el = case (sorted, el) of
    ([], el) -> [el] 
    (x:xs, el) 
        | el >= x -> x:insertSorted xs el
        | otherwise ->  el:x:xs


findIndices ::(Int -> Bool) -> [Int] -> [Int] 
findIndices pred xs = 
    [ snd pair | pair <- (zip xs [0..]), (pred (fst pair))]


allReverse :: [String] -> [String]
allReverse xs = reverse (map reverse xs)

noDigits :: String -> String
noDigits str = 
    let digits = "0123456789" in
        filter (not . (`elem` digits)) str 


cntGood :: [Int -> Bool] -> Int -> Int
cntGood pred x = sum (map 
    (\p -> if p x then 1 else 0) pred)

triangle :: [Integer]
triangle  = scanl1 (+) [1..]

pyramid :: [Integer]
pyramid  = scanl1 (+) (map (^ 2)  [1..])

-- i tested this in previous project
-- first element is factorial of 1, not zero - [1, 2, 6...]
factorialsM :: [Integer]
factorialsM =  1 : zipWith (*) factorialsM [2..]


