module FindIndices where

import Test.Hspec
import TestTemplate
import HW02

findIndicesTests = hspec $ do
    describe "FindIndices" $ do
        subTest "no present"
            (findIndices even [1, 3, 5] `shouldBe` [])

        subTest "empty list"
            (findIndices even [] `shouldBe` [])

        subTest "general case"
            (findIndices even [1, 2, 3, 4, 5] `shouldBe` [1, 3])