module Triangle where

import Test.Hspec
import TestTemplate
import HW02
        
triangleTests = hspec $ do
    describe "Triangle" $ 
        subTest "general case"
            (take 5 triangle `shouldBe` [1, 3, 6, 10, 15])