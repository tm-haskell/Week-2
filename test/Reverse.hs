module Reverse where

import Test.Hspec
import TestTemplate
import HW02
    
reverseTests = hspec $ do
    describe "Reverse" $ do

        subTest "empty"
            (allReverse [] `shouldBe` [])

        subTest "one list"
            (allReverse ["123"] `shouldBe` ["321"])

        subTest "general case"
            (allReverse ["123", "11-12-13"] `shouldBe` ["31-21-11", "321"])