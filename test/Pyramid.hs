module Pyramid where

import Test.Hspec
import HW02
import TestTemplate
        
pyramidTests = hspec $ do
    describe "Pyramid" $
    
        subTest "general case"
            (take 5 pyramid `shouldBe` [1, 5, 14, 30, 55] )

