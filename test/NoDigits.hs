module NoDigits where

import Test.Hspec
import TestTemplate
import HW02
        
noDigitsTests = hspec $ do
    describe "NoDigits" $ do

        subTest "empty"
            (noDigits [] `shouldBe` [])

        subTest "no digits present"
            (noDigits "abc" `shouldBe` "abc")

        subTest "digits present"
            (noDigits "a1b2c" `shouldBe` "abc")
        
        