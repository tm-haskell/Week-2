module CntGood where

import Test.Hspec
import TestTemplate
import HW02
        
cntGoodTests = hspec $ do
    describe "CntGood" $ do

        subTest "no preds"
            (cntGood [] 42 `shouldBe` 0)

        subTest "one matching pred"
            (cntGood [even] 42 `shouldBe` 1)
        
        subTest "no matching preds"
            (cntGood [odd] 42 `shouldBe` 0)
        
        subTest "multiple preds"
            (cntGood [odd, even, \x -> x - 10 == 32] 42 `shouldBe` 2)
        