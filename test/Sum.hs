module Sum where 

import Test.Hspec
import HW02
import TestTemplate

sumTests = hspec $ do
    describe "Sum" $ do
        subTest "Sum empty case"
            (sumTest [])

        subTest "Sum single element case"
            (sumTest [4])
        
        subTest "Sum general case"
            (sumTest [4, 4, 2])


sumTest ls =  sumFl ls `shouldBe` sum ls