module Main  where

import Sum
import Product
import Concat
import SortInsert
import FindIndices
import Reverse
import NoDigits
import CntGood
import Triangle
import Pyramid

main :: IO ()
main = all_tests

all_tests :: IO()
all_tests = do 
    -- sumTests
    -- productTests
    -- concatTests
    -- sortTests
    -- findIndicesTests
    -- reverseTests
    -- noDigitsTests
    -- cntGoodTests
    -- triangleTests
    pyramidTests
