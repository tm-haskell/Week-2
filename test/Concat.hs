module Concat where 

import Test.Hspec
import TestTemplate
import HW02

concatTests = hspec $ do
    describe "Concat" $ do
        subTest "Concat empty case"
            (concatTestCode [] [1])

        subTest "Concat single element case"
            (concatTestCode [4] [3])
        
        subTest "Concat general case"
            (concatTestCode [4, 4, 1] [1, 4, 4])

concatTestCode :: [Int] -> [Int] -> Expectation
concatTestCode left right =  
    concatFr left right `shouldBe` left ++ right