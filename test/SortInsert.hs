module SortInsert where

import Test.Hspec
import TestTemplate
import HW02
import Data.List

sortTests = hspec $ do
    describe "SortInsert" $ do
        subTest "insertSorted general case"
            (insertSorted [1, 2, 4] 3 `shouldBe` [1, 2, 3, 4])
        
        subTest "SortInsert empty case"
            (sortTestCode [])

        subTest "SortInsert single element case"
            (sortTestCode [42])
        
        subTest "SortInsert general case"
            (sortTestCode [42, 13, 17, 250, 2, 3])

sortTestCode :: [Int] -> Expectation
sortTestCode xs = sortInsert xs `shouldBe` sort xs