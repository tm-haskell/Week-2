module Product where 

import Test.Hspec
import HW02
import TestTemplate

productTests = hspec $ do
    describe "Product" $ do
        subTest "Product empty case"
            (productTestCode [])

        subTest "Product single element case"
            (productTestCode [4])
        
        subTest "Product general case"
            (productTestCode [4, 4, 2])


productTestCode ls =  productFr ls `shouldBe` product ls